﻿// Copyright (c) 2013 The TuxLoL authors.
// 
// This file is part of TuxLoL.
// 
// TuxLoL is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// TuxLoL is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with TuxLoL.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace Xargoth.TuxLoL.Core
{
    public class RiotArchiveFile
    {
        public const int MagicNumber = 0x18be0ef0;

        public RiotArchiveFile(int version, int managerIndex, List<RiotArchiveFileListEntry> fileList)
        {
            if (fileList == null)
            {
                throw new ArgumentNullException("fileList");
            }

            Version = version;
            ManagerIndex = managerIndex;
            FileList = fileList;
        }

        public int Version { get; private set; }

        public int ManagerIndex { get; private set; }

        public List<RiotArchiveFileListEntry> FileList { get; private set; }
    }
}